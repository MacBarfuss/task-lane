FROM python:3.6

ENV PYTHONUNBUFFERED 1  

RUN apt-get update && \
    apt-get install --yes python3-dev && \
    apt-get install --yes default-libmysqlclient-dev

RUN mkdir /config

ADD /requirements_dev.txt /config/
ADD /requirements_prod.txt /config/

RUN pip install -r /config/requirements_dev.txt
RUN pip install -r /config/requirements_prod.txt

RUN mkdir /src

WORKDIR /src
