#!/bin/bash

if [ -z "$TASK_LANE_STAGE" ]
then
	TASK_LANE_STAGE=Dev
fi

echo "Updating the $TASK_LANE_STAGE Environment"

python3 -m pip install -r requirements.txt

python3 manage.py collectstatic --noinput --configuration $TASK_LANE_STAGE

python3 manage.py migrate --configuration $TASK_LANE_STAGE

sudo systemctl restart task-lane.$TASK_LANE_STAGE.service
sudo systemctl restart task-lane.$TASK_LANE_STAGE.socket
