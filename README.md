# Task-Lane

The todo list which keeps you on a clear track.

Current stable version: v0.19

## Setup

At the moment the setup is completely manual.

### Installation

#### Prerequisites

### Upgrading

Upgrading from any develop-version is supported since v0.5.
Upgrading from any master-version is supported.

#### Checklist for creating a release

1. Start a new release with git-flow.
1. Run `makemigrations` to check if a change was not applied yet. Migrate if necessary.
1. Squash migrations and note in the README.md if an upgrade step is no longer supported. Make a commit!
1. Document the new release number
  1. in the head of README.md
  1. in the about.html.
  1. Make a commit "bump the version number."
1. Run all tests and deployment-checks. Commit all fixes!
1. Finish the new release an push to origin.
1. Close the milestone.
1. Set the version-number in about.html to next minor version number plus "-SNAPSHOT"-ending.

#### Checklist for updating an instance to a new release

1. git pull in the project directory.
1. `python3 -m pip install -r requirements.txt`
1. `python3 manage.py collectstatic --noinput --configuration Prod`
1. `python3 manage.py migrate --configuration Prod`
1. `systemctl restart tasks.gunicorn.service`
1. `systemctl restart tasks.gunicorn.socket`

## Concepts

**Scope** Largest grouping of items. This is used to seperate private from business-related stuff for example.

**Project** Used as one layer of subscope. Open Idea: Use this to make planning and reporting.

**Milestone** To set a target on time, track it and later to review the done tasks use milestones. Done tasks are hidden when their milestone is "happened".

### Support for Getting-Things-Done

**Context**

**Status**
