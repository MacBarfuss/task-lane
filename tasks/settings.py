"""
Django settings for tasks project.


"""

import os

from configurations import Configuration

class Base(Configuration):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # Security
    # TODO externalize this to environment
    SECRET_KEY = 'klffck=#or+aidke#mwd30w2yfg4xh01*d6gx-g3vx#578u#!7'

    DEBUG = False

    INSTALLED_APPS = [
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'bootstrap4',
        'bootstrap_datepicker_plus',
        'django_activeurl',
        'django_filters',
        'pomodoro.apps.PomodoroConfig',
        'tasksapp.apps.TasksappConfig',

        # Health Check
        'health_check',
        'health_check.db',
        'health_check.cache',
        'health_check.storage',
    ]

    MIDDLEWARE = [
        'django.middleware.security.SecurityMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    ]
    
    ROOT_URLCONF = 'tasks.urls'
    
    TEMPLATES = [
        {
            'BACKEND': 'django.template.backends.django.DjangoTemplates',
            'DIRS': [os.path.join(BASE_DIR, 'tasks/templates')],
            'APP_DIRS': True,
            'OPTIONS': {
                'context_processors': [
                    'django.template.context_processors.debug',
                    'django.template.context_processors.request',
                    'django.contrib.auth.context_processors.auth',
                    'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]
    
    WSGI_APPLICATION = 'tasks.wsgi.application'
 
    AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
    ]

    # Internationalization
    LANGUAGE_CODE = 'de-de'
    TIME_ZONE = 'UTC'
    USE_I18N = True
    USE_L10N = True    
    USE_TZ = True
    
    # Static files (CSS, JavaScript, Images)
    STATIC_URL = '/static/'
    STATICFILES_DIRS = (
        os.path.join(BASE_DIR, 'staticfiles'),
    )
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    
    # UI stuff
    BOOTSTRAP4 = {
        'include_jquery': True,
    }


class Dev(Base):
    DEBUG = True
    ALLOWED_HOSTS = ['*']
    
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(Base.BASE_DIR, 'db.sqlite3'),
        }
    }


class Staging(Base):
    DEBUG = True
    ALLOWED_HOSTS = ['*']

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'task_lane_staging',
            'USER': 'tasks',
            'PASSWORD': 'ei8tiesush6ahK6beiNath4EoraiQuiz',
            'HOST': 'localhost',
            'PORT': '5432',
        }
    }
    
class Prod(Base):
    ALLOWED_HOSTS = ['localhost']

    DATABASES = {
     'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'tasks',
        'USER': 'tasks',
        'PASSWORD': 'ei8tiesush6ahK6beiNath4EoraiQuiz',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
