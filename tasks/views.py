from django.conf import settings
from django.views.generic.base import TemplateView

class About(TemplateView):
    """View for displaying Information about the service."""
    template_name = 'about.html'
    
    def get_context_data(self, **kwargs):
        """context got enriched to show debugging information."""

        context = super(About, self).get_context_data(**kwargs)

        context['database'] = settings.DATABASES

        return context
