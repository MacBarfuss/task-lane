from datetime import date

from django.contrib.auth.models import User

from django.db import models

from django.urls import reverse
from django.utils import timezone

class Category(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    def __str__(self):
        return self.name

class Context(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('context-edit', kwargs={'pk': self.pk})

class Goal(models.Model):
    name = models.CharField(max_length = 20)
    simple = models.TextField()
    measurable = models.TextField()
    # TODO add metric links
    actionable = models.TextField()
    # TODO add links to tasks as 'actions'
    reasonable = models.TextField()
    timebound = models.DateField()

    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('goal-edit', kwargs={'pk': self.pk})

class Scope(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('scope-edit', kwargs={'pk': self.pk})

class UserSettings(models.Model):
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    pomodoro_duration = models.PositiveSmallIntegerField(default = 25)
    def __str__(self):
        return str(self.user)
    def get_absolute_url(self):
        return reverse('usersettings-edit')

class Project(models.Model):
    name = models.CharField(max_length=20)
    active = models.BooleanField(default=True)
    description = models.TextField(blank=True)
    scope = models.ForeignKey(Scope, blank = True, null = True, on_delete = models.PROTECT)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('project-edit', kwargs={'pk': self.pk})

class Milestone(models.Model):
    name = models.CharField(max_length=20)
    project = models.ForeignKey(Project, blank = True, null = True, on_delete = models.SET_NULL)
    datetime_to_pass = models.DateTimeField(default=timezone.now)
    description = models.TextField(blank=True)
    happened = models.BooleanField(default=False)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('milestone-edit', kwargs={'pk': self.pk})

class Status(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField(blank=True)
    is_done = models.BooleanField(default = False)
    is_new = models.BooleanField(default = False)
    def __str__(self):
        return self.name
    def get_absolute_url(self):
        return reverse('status-edit', kwargs={'pk': self.pk})

class TaskEnhancedManager(models.Manager):
    def create(self, *args, **kwargs):
        task = models.Manager.create(self, *args, **kwargs)
        task.full_clean()
        return task

class Task(models.Model):
    objects = TaskEnhancedManager()

    # basic information
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    dateCreated = models.DateField(default=date.today)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    shared = models.BooleanField(default=False)

    # categorisation
    categories = models.ManyToManyField(Category, blank=True)
    context = models.ForeignKey(Context, blank = True, null = True, on_delete = models.SET_NULL)
    goals = models.ManyToManyField(Goal, blank = True)
    project = models.ForeignKey(Project, blank = True, null = True, on_delete = models.CASCADE)
    scope = models.ForeignKey(Scope, blank = True, null = True, on_delete = models.PROTECT)

    # state
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    assignee = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='assigned_tasks')
    milestone = models.ForeignKey(
        Milestone,
        on_delete = models.PROTECT,
        null = True,
        blank = True)

    @property
    def blocked(self):
        blockers = self.get_all_transient_blockers()
        for blocker in blockers:
            if not blocker.status.is_done:
                return True
        return False

    # generators
    @staticmethod
    def get_initials(request):
        try:
            sticky_project_pk = request.session['sticky_project']
        except:
            sticky_project_pk = None

        try:
            sticky_scope_pk = request.session['sticky_scope']
        except:
            sticky_scope_pk = None

        return {
            'owner': request.user,
            'project': sticky_project_pk,
            'scope': sticky_scope_pk,
            'status': Status.objects.filter(is_new = True).first()
        }

    # priority
    due_date = models.DateField(null = True, blank = True)
    priorityPoints = models.PositiveSmallIntegerField(default=1)
    priority_sum = models.IntegerField(default = 1, editable = False)

    def calc_priority_sum(self):
        result = self.priorityPoints
        if self.id is not None:
            dependants = Task.objects.filter(dependencies = self)
            for dependant in dependants:
                result += dependant.priority_sum
        return result

    # dependency
    dependencies = models.ManyToManyField('Task', blank = True, related_name = 'blocker')

    def update_priority_sum(self):
        self.priority_sum = self.calc_priority_sum()
        self.save()

    def get_all_transient_blockers(self):
        # TODO rename to "dependencies"
        all_transient_blockers = set()
        self_dependants = self.dependencies.all()
        for self_dependant in self_dependants:
            if self_dependant not in all_transient_blockers:
                all_transient_blockers.add(self_dependant)
                all_transient_blockers.update(self_dependant.get_all_transient_blockers())
        return all_transient_blockers

    def get_all_transient_dependants(self):
        all_transient_dependants = set()
        self_blockers = Task.objects.filter(dependencies = self)
        for self_blocker in self_blockers:
            if self_blocker not in all_transient_dependants:
                all_transient_dependants.add(self_blocker)
                all_transient_dependants.update(self_blocker.get_all_transient_dependants())
        return all_transient_dependants

    # repeating
    is_repeating = models.BooleanField(default = False)

    # effort
    estimation = models.PositiveSmallIntegerField(default=1)
    logged = models.PositiveSmallIntegerField(default=0)

    def clean(self):
        models.Model.clean(self)
        self.update_priority_sum()

    def save(self, *args, **kwargs):
        super(Task, self).save(*args, **kwargs)
        for dependency in self.dependencies.all():
            dependency.full_clean()
            dependency.save()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('task-edit', kwargs={'pk': self.pk})

class Workflow(models.Model):
    pass