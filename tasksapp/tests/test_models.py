from django.contrib.auth.models import User

from django.test import TestCase

from tasksapp.models import Status, Task
from tasksapp.tests.helpers import TaskBuilder

class TaskTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(TaskTests, cls).setUpTestData()
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.status = Status.objects.create()
        cls.task_builder = TaskBuilder(owner = cls.user, status = cls.status)

    @classmethod
    def setUp(cls):
        TestCase.setUp(cls)
        cls.taskA = cls.task_builder.addTask(name='A', priorityPoints = 5)
        cls.taskB = cls.task_builder.addTask(name='B', priorityPoints = 3)
        cls.taskC = cls.task_builder.addTask(name='C')


    def test_calc_priority_sum_without_dependant(self):
        # GIVEN a simple task with a priority of 5

        # WHEN the priority is changed and the task is stored
        self.taskA.priorityPoints = 2
        self.taskA.full_clean()
        self.taskA.save()

        # THEN the sum gets an update
        self.assertEqual(self.taskA.priority_sum, 2)

    def test_calc_priority_sum_with_one_dependant(self):
        # GIVEN a task with prio 5
        self.assertEqual(self.taskA.priority_sum, 5)
        
        # AND one dependant with prio 3, so B depends on A
        self.assertEqual(self.taskB.priority_sum, 3)
        self.taskB.dependencies.add(self.taskA)
        self.taskB.save()

        # WHEN nothing happens

        # THEN the priority_sum of A is raise by the priority_sum of B
        prospect = Task.objects.get(name = 'A')
        self.assertEqual(prospect.priority_sum, 8)

    def test_calc_priority_sum_with_two_chained_dependants(self):        
        # GIVEN a task with prio 5
        self.assertEqual(self.taskA.priority_sum, 5)
        
        # AND two dependant with prio 3 and 1
        self.taskA.dependencies.add(self.taskB)
        self.taskA.save()
        taskB = Task.objects.get(name = 'B')
        taskB.dependencies.add(self.taskC)
        taskB.save()

        # WHEN nothing happens

        # THEN the sum gets calculated 
        prospect = Task.objects.get(name = 'C')
        self.assertEqual(prospect.priority_sum, 9)

    def test_changing_prio_changes_all_blocking_priority_sums(self):      
        # GIVEN a task with prio 5
        self.assertEqual(self.taskA.priority_sum, 5)
        
        # AND two blockers with prio 3 and 1
        # C depends on B depends on A
        self.taskC.dependencies.add(self.taskB)
        self.taskC.save()
        taskB = Task.objects.get(name = 'B')
        taskB.dependencies.add(self.taskA)
        taskB.save()

        # WHEN the priority of the most dependant rises
        taskC = Task.objects.get(name = 'C')
        taskC.priorityPoints = 11
        taskC.full_clean()
        taskC.save()

        # THEN the blocker priority changes
        prospectB = Task.objects.get(name = 'B')
        self.assertEqual(prospectB.priority_sum, 14)
        prospectA = Task.objects.get(name = 'A')
        self.assertEqual(prospectA.priority_sum, 19)
    
    def test_get_all_transient_blockers_without_blocker_returns_empty_list(self):
        # GIVEN a task without blocker

        # WHEN all transient blockers are fetched
        prospect = self.taskA.get_all_transient_blockers()

        # THEN the list is empty
        self.assertSetEqual(prospect, set([]))

    def test_get_all_transient_blockers_with_one_blocker_returns_this_in_list(self):     
        # GIVEN a task

        # AND one blocking task
        self.taskA.dependencies.add(self.taskB)

        # WHEN all transient blockers are fetched
        prospect = self.taskA.get_all_transient_blockers()

        # THEN the list contains the blocking task
        self.assertSetEqual(prospect, set([self.taskB]))

    def test_get_all_transient_blockers_with_two_blockers_returns_them_in_list(self):     
        # GIVEN a task

        # AND two blocking tasks
        self.taskA.dependencies.add(self.taskB)
        self.taskA.dependencies.add(self.taskC)

        # WHEN all transient blockers are fetched
        prospect = self.taskA.get_all_transient_blockers()

        # THEN the list contains the blocking task
        self.assertSetEqual(prospect, set([self.taskB, self.taskC]))

    def test_get_all_transient_blockers_with_two_chained_blockers_returns_both_in_list(self):     
        # GIVEN a task

        # AND two blocking tasks
        self.taskA.dependencies.add(self.taskB)
        self.taskB.dependencies.add(self.taskC)

        # WHEN all transient blockers are fetched
        prospect = self.taskA.get_all_transient_blockers()

        # THEN the list contains the blocking task
        self.assertSetEqual(prospect, set([self.taskC, self.taskB]))
    
    def test_get_all_transient_dependants_without_dependant_returns_empty_list(self):
        # GIVEN a task without blocker
        # WHEN all transient blockers are fetched
        prospect = set(self.taskA.get_all_transient_dependants())

        # THEN the list is empty
        self.assertSetEqual(prospect, set([]))

    def test_get_all_transient_dependants_with_one_dependant_returns_this_in_list(self):     
        # GIVEN a task

        # AND one dependant task
        self.taskB.dependencies.add(self.taskA)

        # WHEN all transient dependants are fetched
        prospect = set(self.taskA.get_all_transient_dependants())

        # THEN the list contains the dependant task
        self.assertSetEqual(prospect, set([self.taskB]))

    def test_get_all_transient_dependants_with_two_dependants_returns_them_in_list(self):     
        # GIVEN a task

        # AND two dependant tasks
        self.taskB.dependencies.add(self.taskA)
        self.taskC.dependencies.add(self.taskA)

        # WHEN all transient dependants are fetched
        prospect = set(self.taskA.get_all_transient_dependants())

        # THEN the list contains the dependant task
        self.assertSetEqual(prospect, set([self.taskB, self.taskC]))

    def test_get_all_transient_dependants_with_two_chained_dependants_returns_both_in_list(self):     
        # GIVEN a task

        # AND two blocking tasks
        self.taskB.dependencies.add(self.taskA)
        self.taskC.dependencies.add(self.taskB)

        # WHEN all transient blockers are fetched
        prospect = set(self.taskA.get_all_transient_dependants())

        # THEN the list contains the dependant task
        self.assertSetEqual(prospect, set([self.taskB, self.taskC]))