from django.contrib.auth.models import User

from django.test import TestCase

from tasksapp.models import Status, Task
from tasksapp.tests.helpers import TaskBuilder

class Instatiation(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(Instatiation, cls).setUpTestData()
        cls.task_builder = TaskBuilder()
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.status = Status.objects.create()

    def test_create_priority_sum_is_correct(self):
        taskA = Task.objects.create(name='A', owner=self.user, status=self.status, priorityPoints=5)
        self.assertEqual(5, taskA.priorityPoints)
        self.assertEqual(5, taskA.priority_sum)
        
    def test_priority_sum_is_set(self):
        taskA = self.task_builder.addTask(name='A', owner=self.user, status=self.status, priorityPoints = 5)
        self.assertEqual(5, taskA.priorityPoints)
        self.assertEqual(5, taskA.priority_sum)

    def test_two_independent_tasks_has_correct_priority(self):
        taskA = self.task_builder.addTask(name='A', owner=self.user, status=self.status, priorityPoints = 5)
        taskB = self.task_builder.addTask(name='B', owner=self.user, status=self.status, priorityPoints = 3)
        self.assertEqual(5, taskA.priorityPoints)
        self.assertEqual(5, taskA.priority_sum)
        self.assertEqual(3, taskB.priorityPoints)
        self.assertEqual(3, taskB.priority_sum)
    
class FieldBlocked(TestCase):
    @classmethod
    def setUpTestData(cls):
        super(FieldBlocked, cls).setUpTestData()
        cls.task_builder = TaskBuilder()
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.status = Status.objects.create()

    @classmethod
    def setUp(cls):
        cls.taskA = cls.task_builder.addTask(name='A', owner=cls.user, status=cls.status)

    def test_unblocked_task_has_blocked_false(self):
        # THEN
        self.assertFalse(self.taskA.blocked)

    def test_blocked_task_has_blocked_true(self):
        # GIVEN a blocked task
        taskB = self.task_builder.addTask(name='B', owner=self.user, status=self.status)
        taskB.dependencies.add(self.taskA)

        # THEN
        self.assertFalse(self.taskA.blocked)
        self.assertTrue(taskB.blocked)
