from django.contrib.auth.models import User
from django.test import Client, TestCase

from tasksapp.models import Milestone, Status, Project
from tasksapp.tests.helpers import TaskBuilder

class FinishTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)
        cls.status_new = Status.objects.create(is_new = True)
        cls.status_done = Status.objects.create(is_done = True)
    
    def test_view_opens_without_error(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        # GIVEN one milestone
        Milestone.objects.create(name = 'v1')

        # WHEN the finish view is opened
        response = client.get('/tasks/milestone/1/finish/')
    
        # THEN there is no error
        self.assertEqual(response.status_code, 200)

    def test_submit_form_on_empty_milestone_succeeds(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        # GIVEN one milestone
        Milestone.objects.create(name = 'v1')

        # WHEN the finish view is submitted
        response = client.post('/tasks/milestone/1/finish/')
    
        # THEN there is no error
        self.assertEqual(response.status_code, 200)

    def test_submit_form_on_milestone_with_closed_tasks_succeeds(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        # GIVEN one milestone
        milestone = Milestone.objects.create(name = 'v1')
        # AND two done tasks assigned to this milestone
        self.task_builder.addTask(status = self.status_done, milestone = milestone)
        self.task_builder.addTask(status = self.status_done, milestone = milestone)

        # WHEN the finish view is submitted
        response = client.post('/tasks/milestone/1/finish/')
    
        # THEN there is no error
        self.assertEqual(response.status_code, 200)

    def test_submit_form_on_milestone_with_open_tasks(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        # GIVEN one milestone
        milestone = Milestone.objects.create(name = 'v1')
        # AND two new tasks assigned to this milestone
        self.task_builder.addTask(status = self.status_new, milestone = milestone)
        self.task_builder.addTask(status = self.status_new, milestone = milestone)

        # WHEN the finish view is submitted
        response = client.post('/tasks/milestone/1/finish/')
    
        # THEN there is no error
        self.assertEqual(response.status_code, 200)

class SetupTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)

    def setUp(self):
        TestCase.setUp(self)
        # GIVEN two projects
        self.projectA = Project.objects.create(name = 'ProjectA')
        self.projectB = Project.objects.create(name = 'ProjectB')
        
        # AND three tasks, two in separate projects, one without project
        self.taskA = self.task_builder.addTask(name = 'A', project = self.projectA)
        self.taskB = self.task_builder.addTask(name = 'B', project = self.projectB)
        self.taskC = self.task_builder.addTask(name = 'C')

    def test_with_project_shows_only_tasks_of_same_project(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')
        
        # GIVEN a milestone for project A
        milestone = Milestone.objects.create(project = self.projectA)
        
        # WHEN setup view is opened
        response = client.get('/tasks/milestone/' + str(milestone.pk) + '/setup')
        
        # THEN the task list only shows task A
        task_list = response.context_data['task_list']
        self.assertIn(self.taskA, task_list)
        self.assertEqual(1, task_list.count())
    
    def test_setup_milestone_without_project_shows_all_tasks(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')
        
        # GIVEN a milestone without project
        milestone = Milestone.objects.create()
        
        # WHEN setup view is opened
        response = client.get('/tasks/milestone/' + str(milestone.pk) + '/setup')
        
        # THEN all unfinished tasks are displayed
        task_list = response.context_data['task_list']
        self.assertIn(self.taskA, task_list)
        self.assertIn(self.taskB, task_list)
        self.assertIn(self.taskC, task_list)
        self.assertEqual(3, task_list.count())