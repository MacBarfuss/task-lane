from django.contrib.auth.models import User
from django.test import Client, TestCase, tag

from tasksapp.models import Project

class CreateTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
    
    def test_view_opens_without_error(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        client.get('/tasks/project/add/')
    
    def test_view_opens_without_error_if_sticky_project_is_set(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')
        
        # GIVEN a sticky project
        project = Project.objects.create(name = 'Project A')
        client.get('/tasks/sticky/project/' + str(project.pk) + '/set')

        client.get('/tasks/project/add/')


@tag('wip')
class ListTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')

    def test_only_active_showed(self):
        client = Client()
        client.login(username='tester', password='secret')

        # GIVEN two projects, one archived
        active_project = Project.objects.create(name='Project B')
        old_project = Project.objects.create(name='Project A', active=False)

        # WHEN the view opens
        response = client.get('/tasks/project/')

        # THEN the list only contains the active project
        project_list = response.context_data['project_list']
        self.assertEqual(len(project_list), 1)
