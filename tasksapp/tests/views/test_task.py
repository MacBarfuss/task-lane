from django.contrib.auth.models import User

from django.test import Client, TestCase

from tasksapp.models import Status, Task, Scope, Project
from tasksapp.tests.helpers import TaskBuilder

class TaskCreateTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
    
    def test_create_task_when_scope_is_sticky_prefills_scope_to_form(self):
        client = Client()
        client.login(username = 'tester', password = 'secret')

        # GIVEN a sticky scope
        scope = Scope.objects.create(name = 'personal')
        client.get('/tasks/sticky/scope/' + str(scope.pk) + '/set')

        # WHEN add task form is opened
        response = client.get('/tasks/task/add/')

        # THEN its scope is set to the sticky scope
        self.assertEqual(response.context_data['form'].initial['scope'], scope.pk)

class TaskSplitTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)
    
    def setUp(self):
        TestCase.setUp(self)
        self.client = Client()
        self.client.login(username = 'tester', password = 'secret')

    def test_split_task_adds_new_task_with_first_task_as_dependant(self):
        # GIVEN a task
        task = self.task_builder.addTask()
        
        # WHEN split task is performed
        response = self.client.post('/tasks/task/' + str(task.pk) + '/split', {'name': 'splitted task'})

        # THEN database has the new task
        new_task = Task.objects.get(name = 'splitted task')
        self.assertIsNotNone(new_task)
        self.assertIn(task, new_task.get_all_transient_dependants())
        
    def test_split_task_when_project_is_sticky_adds_project_to_task(self):
        # GIVEN a sticky project
        project = Project.objects.create(name = 'the project')
        self.client.get('/tasks/sticky/project/' + str(project.pk) + '/set')

        # AND a task
        task = self.task_builder.addTask()
        
        # WHEN split task form is opened and submitted
        self.client.post('/tasks/task/' + str(task.pk) + '/split', {'name': 'splitted task'})

        # THEN its project is set to the sticky project
        prospect = Task.objects.get(name = 'splitted task')
        self.assertEqual(prospect.project, project)

class TaskPrioUpTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # GIVEN
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)
        cls.status_new = Status.objects.create(is_new = True)
        cls.status_done = Status.objects.create(is_done = True)

    def test_prio_up_ignores_done_tasks(self):
        # GIVEN three tasks with highest undone
        taskC = self.task_builder.addTask(name = 'C', status = self.status_new, priorityPoints = 5)
        # second done
        taskB = self.task_builder.addTask(name = 'B', status = self.status_done, priorityPoints = 3)
        # and third undone
        taskA = self.task_builder.addTask(name = 'A', status = self.status_new)

        # WHEN the prio of taskA is raised
        client = Client()
        client.login(username = 'tester', password = 'secret')
        client.get('/tasks/task/' + str(taskA.pk) + '/prio-up')
        
        # THEN the priorities are 5, 3 and 6 for the three tasks
        self.assertEqual(taskC.priorityPoints, 5)
        self.assertEqual(taskB.priorityPoints, 3)
        self.assertEqual(Task.objects.get(name = 'A').priorityPoints, 6)

class AddDependencyTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('tester', 'test@gema-soft.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)
    
    def test_add_self_as_dependency_avoided_with_message(self):
        # GIVEN one simple task
        task = self.task_builder.addTask()

        # WHEN the task should get itself as dependency
        client = Client()
        client.login(username = 'tester', password = 'secret')
        client.get('/tasks/task/' + str(task.pk) + '/add-dependency/' + str(task.pk) + '/')

        # THEN the dependency is not added
        savedTask = Task.objects.get(pk = task.pk)
        self.assertEquals(savedTask.dependencies.count(), 0)

    def test_add_cyclic_dependency_avoided(self):
        # GIVEN three simple tasks A, B and C
        taskA = self.task_builder.addTask(name = 'A')
        taskB = self.task_builder.addTask(name = 'B')
        taskC = self.task_builder.addTask(name = 'C')
        
        # AND A depends on B depends on C
        taskA.dependencies.add(taskB)
        taskA.save()
        taskB.dependencies.add(taskC)
        taskB.save()
        
        # WHEN C should now depend on A
        client = Client()
        client.login(username = 'tester', password = 'secret')
        client.get('/tasks/task/' + str(taskC.pk) + '/add-dependency/' + str(taskA.pk) + '/')
        
        # THEN the dependency is not added
        savedTask = Task.objects.get(name = 'C')
        self.assertEquals(savedTask.dependencies.count(), 0)