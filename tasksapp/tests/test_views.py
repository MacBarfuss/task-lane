from django.contrib.auth.models import User

from django.test import Client, TestCase

from tasksapp.models import Task
from tasksapp.tests.helpers import TaskBuilder

class AllTaskListViewTests(TestCase):

    @classmethod
    def addTask(cls, name, owner, status, priorityPoints):
        new_task = Task.objects.create(name=name, owner=owner, status=status, priorityPoints=priorityPoints) #: :type new_task: Task
        new_task.full_clean()
        new_task.save()
        return new_task
        
    @classmethod
    def setUpTestData(cls):
        # GIVEN
        cls.user = User.objects.create_user('tester', 'test@macbarfuss.de', 'secret')
        cls.task_builder = TaskBuilder(owner = cls.user)
        cls.task_builder.addTask(name='A', priorityPoints = 5)

    def test_task_has_priority_sum(self):
        # WHEN the AllTaskList is viewed
        client = Client()
        client.login(username = 'tester', password = 'secret')
        response = client.get('/tasks/task/all/')

        # THEN there is a priority sum of 5
        task = response.context['task_list'].get(name = 'A') #: :type task: Task
        self.assertEqual(task.priority_sum, 5)
