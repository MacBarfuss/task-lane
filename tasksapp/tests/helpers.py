import uuid

from tasksapp.models import Task, Status

class TaskBuilder():
    '''This builder helps in creating valid task objects.
    
    Parameters common to all tasks to be created can be passed to the constructor as named arguments.
    
    If a parameter is passed when building a task this parameter has higher precedence over a common value.
    
    The owner must be provided when constructing the builder or when building a task.
    
    Other fields get filled with generated data if it is not provided.
    '''

    def __init__(self, **kwargs):
        self.owner = kwargs.get('owner')
        self.priorityPoints = kwargs.get('priorityPoints', 1)
        self.status = kwargs.get('status', Status.objects.create(is_new = True))
        
    def addTask(self, **kwargs):
        kwargs['name'] = kwargs.get('name', str(uuid.uuid4()))
        kwargs['owner'] = kwargs.get('owner', self.owner)
        kwargs['priorityPoints'] = kwargs.get('priorityPoints', self.priorityPoints)
        kwargs['status'] = kwargs.get('status', self.status)

        new_task = Task.objects.create(**kwargs) #: :type new_task: Task
        new_task.save()
        
        return Task.objects.get(name = kwargs['name'])
