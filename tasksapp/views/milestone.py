from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist

from django.shortcuts import redirect

from django.urls import reverse_lazy

from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from django_filters.views import FilterView

from tasksapp.forms import MilestoneForm, MilestoneFinishForm
from tasksapp.models import Milestone, Task

class MilestoneCreate(LoginRequiredMixin, CreateView):
    form_class = MilestoneForm
    model = Milestone
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Milestone created successfully.')
        return super().form_valid(form)

class MilestoneDelete(LoginRequiredMixin, DeleteView):
    model = Milestone
    success_url = '/tasks/milestone/?happened=3'

class MilestoneFinish(LoginRequiredMixin, UpdateView):
    model = Milestone
    form_class = MilestoneFinishForm
    template_name = 'tasksapp/milestone/finish.html'
    success_url = '/tasks/milestone/?happened=3'

    def get_context_data(self, **kwargs):
        context = super(MilestoneFinish, self).get_context_data(**kwargs)
        context['task_list'] = Task.objects.filter(milestone = context['milestone']).order_by('-priority_sum').order_by('status__is_done')
        return context
    
    def form_valid(self, form):
        '''the milestone gets marked happened automatically.'''
        milestone = self.object

        undone_task_list = Task.objects.filter(milestone = milestone).filter(status__is_done = False)
        for undone_task in undone_task_list:
            messages.info(self.request, 'Some tasks removed from milestone. Their own priority got doubled.')
            undone_task.milestone = None
            undone_task.priorityPoints *= 2
            undone_task.clean()
            undone_task.save()

        # add success message
        messages.success(self.request, 'Milestone happened.')
        
        milestone.happened = True
        milestone.save()

        return super().form_valid(form)

class MilestoneList(LoginRequiredMixin, FilterView):
    model = Milestone
    filterset_fields = ['happened', 'project']

    def get_context_data(self, **kwargs):
        context = super(MilestoneList, self).get_context_data(**kwargs)
        context['object_list'] = context['object_list'].order_by('datetime_to_pass')
        return context

class MilestoneUpdate(LoginRequiredMixin, UpdateView):
    form_class = MilestoneForm
    model = Milestone
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Milestone saved successfully.')
        return super().form_valid(form)

class Report(LoginRequiredMixin, DetailView):
    model = Milestone
    template_name = 'tasksapp/milestone/report.html'

    def get_context_data(self, **kwargs):
        context = super(Report, self).get_context_data(**kwargs)
        context['task_list'] = Task.objects.filter(milestone = context['milestone']).order_by('-priority_sum')
        return context

class Setup(LoginRequiredMixin, DetailView):
    model = Milestone
    template_name = 'tasksapp/milestone/setup.html'

    def get_context_data(self, **kwargs):
        context = super(Setup, self).get_context_data(**kwargs)
        task_list = Task.objects.filter(milestone = None)
        project = context['object'].project
        if project is not None:
            task_list = task_list.filter(project = project)
        task_list = task_list.order_by('-priority_sum')
        context['task_list'] = task_list
        return context

def setup_add_task(request, milestone_pk, task_pk):
    try:
        task = Task.objects.get(pk = task_pk)
        milestone = Milestone.objects.get(pk = milestone_pk)
        task.milestone = milestone
        task.save()
    except ObjectDoesNotExist:
        messages.error(request, 'Either the task or the milestone does not exist.')

    return redirect('milestone-setup', milestone_pk)