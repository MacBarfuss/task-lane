from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView

from tasksapp.models import Scope, Goal
from django.views.generic.list import ListView
from tasksapp.forms import GoalForm

class CreateView(LoginRequiredMixin, CreateView):
    model = Goal
    form_class = GoalForm
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Goal created successfully.')
        return super().form_valid(form)

class UpdateView(LoginRequiredMixin, UpdateView):
    model = Goal
    form_class = GoalForm
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Goal saved successfully.')
        return super().form_valid(form)

class DeleteView(LoginRequiredMixin, DeleteView):
    model = Goal
    success_url = reverse_lazy('goal-list')

class ListView(LoginRequiredMixin, ListView):
    model = Goal

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        if 'sticky_scope' in self.request.session:
            context['sticky_scope'] = Scope.objects.get(pk = self.request.session['sticky_scope'])
        return context
