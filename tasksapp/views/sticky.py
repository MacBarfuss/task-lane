from django.shortcuts import redirect

def sticky_project_set(request, project_pk):
    request.session['sticky_project'] = project_pk
    return redirect('/tasks/task/my/?archived=3&status=&scope=')

def sticky_project_unset(request):
    del request.session['sticky_project']
    
    try:
        next_url = request.GET['next']
    except:
        next_url = '/tasks/task/my/?archived=3&status=&scope='

    return redirect(next_url)

def sticky_scope_set(request, scope_pk):
    request.session['sticky_scope'] = scope_pk
    return redirect('/tasks/task/my/?archived=3&status=&scope=')

def sticky_scope_unset(request):
    del request.session['sticky_scope']
    
    try:
        next_url = request.GET['next']
    except:
        next_url = '/tasks/task/my/?archived=3&status=&scope='

    return redirect(next_url)
