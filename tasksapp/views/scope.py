from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView

from tasksapp.models import Scope
from django.views.generic.list import ListView

class CreateView(LoginRequiredMixin, CreateView):
    model = Scope
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Scope created successfully.')
        return super().form_valid(form)

class UpdateView(LoginRequiredMixin, UpdateView):
    model = Scope
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Scope saved successfully.')
        return super().form_valid(form)

class DeleteView(LoginRequiredMixin, DeleteView):
    model = Scope
    success_url = reverse_lazy('scope-list')

class ListView(LoginRequiredMixin, ListView):
    model = Scope

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        if 'sticky_scope' in self.request.session:
            context['sticky_scope'] = Scope.objects.get(pk = self.request.session['sticky_scope'])
        return context
