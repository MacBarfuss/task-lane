from datetime import datetime, timedelta

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin

from django.db.models import F, Q

from django.http import HttpResponse

from django.shortcuts import redirect

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView,\
    FormView
from django.views.generic import ListView

from django_filters.views import FilterView

from tasksapp.forms import TaskForm, TaskSplitForm
from tasksapp.models import Scope, Status, Task, Project

class TaskCreate(LoginRequiredMixin, CreateView):
    form_class = TaskForm
    model = Task

    def get_initial(self):
        return Task.get_initials(self.request)
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Task created successfully.')
        return super().form_valid(form)

class TaskSplitView(FormView):
    template_name = 'tasksapp/task_split.html'
    form_class = TaskSplitForm
    fields = '__all__'
    success_url = reverse_lazy('my-task-list')

    def get_context_data(self, **kwargs):
        context = super(TaskSplitView, self).get_context_data(**kwargs)
        
        context['dependant'] = Task.objects.get(pk = self.kwargs['task_pk'])
        return context

    def get_initial(self):
        # TODO add more fields directly from parent task
        return {
            'owner': self.request.user
        }
    
    def form_valid(self, form):
        # create the new task
        owner = self.request.user
        status = Status.objects.filter(is_new = True).first()
        
        try:
            project = Project.objects.get(pk = self.request.session['sticky_project'])
        except:
            project = None

        try:
            scope = Scope.objects.get(pk = self.request.session['sticky_scope'])
        except:
            scope = None

        new_blocking_task = Task.objects.create(
            name = self.request.POST['name'],
            owner = owner,
            project = project,
            scope = scope,
            status = status) #: :type new_blocking_task: Task
        self.success_url = new_blocking_task.get_absolute_url()

        dependant = Task.objects.get(pk = self.kwargs['task_pk'])
        dependant.dependencies.add(new_blocking_task)
        dependant.save()

        # add success message
        messages.success(self.request, 'Task created successfully.')
        return super().form_valid(form)
    
class TaskUpdate(LoginRequiredMixin, UpdateView):
    form_class = TaskForm
    model = Task
    def get_context_data(self, **kwargs):
        context = super(TaskUpdate, self).get_context_data(**kwargs)
        context['blocker_list'] = context['task'].get_all_transient_blockers()
        context['dependant_list'] = context['task'].get_all_transient_dependants()
        return context
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Task saved successfully.')
        return super().form_valid(form)

class TaskDelete(LoginRequiredMixin, DeleteView):
    model = Task
    success_url = reverse_lazy('my-task-list')
    
class AllTaskList(LoginRequiredMixin, ListView):
    model = Task
    def get_context_data(self, **kwargs):
        context = super(AllTaskList, self).get_context_data(**kwargs)
        context['task_list'] = Task.objects.filter(Q(owner = self.request.user) | Q(shared = True))
        context['task_list'] = context['task_list'].annotate(
                remainingEffort = F('estimation') - F('logged'))
        context['task_list'] = context['task_list'].order_by('-priority_sum')
        return context

class MyTaskList(LoginRequiredMixin, FilterView):
    model = Task
    filterset_fields = ['project', 'context', 'status', 'scope', 'categories']

    def get_context_data(self, **kwargs):
        context = super(MyTaskList, self).get_context_data(**kwargs)
        context['object_list'] = context['object_list'].filter(owner = self.request.user)
        context['object_list'] = context['object_list'].filter(
            Q(milestone__isnull = True) | Q(milestone__happened = False))
        if 'sticky_project' in self.request.session:
            context['sticky_project'] = Project.objects.get(pk = self.request.session['sticky_project'])
            context['object_list'] = context['object_list'].filter(project = context['sticky_project'])
        if 'sticky_scope' in self.request.session:
            context['sticky_scope'] = Scope.objects.get(pk = self.request.session['sticky_scope'])
            context['object_list'] = context['object_list'].filter(
                Q(scope__isnull = True) | Q(scope = context['sticky_scope']))
        context['object_list'] = context['object_list'].annotate(
                remainingEffort = F('estimation') - F('logged'))
        context['object_list'] = context['object_list'].order_by('-priority_sum')
        return context

class SelectNewDependency(LoginRequiredMixin, FilterView):
    model = Task
    filterset_fields = ['project', 'context', 'status', 'scope']
    template_name = 'tasksapp/task/select_new_dependency.html'

    def get_context_data(self, **kwargs):
        context = super(SelectNewDependency, self).get_context_data(**kwargs)
        context['dependant'] = Task.objects.get(pk = self.kwargs['task_pk'])
        context['object_list'] = context['object_list'].filter(owner = self.request.user)
        context['object_list'] = context['object_list'].filter(
            Q(milestone__isnull = True) | Q(milestone__happened = False))
        if 'sticky_scope' in self.request.session:
            context['sticky_scope'] = Scope.objects.get(pk = self.request.session['sticky_scope'])
            context['object_list'] = context['object_list'].filter(
                Q(scope__isnull = True) | Q(scope = context['sticky_scope']))
        context['object_list'] = context['object_list'].annotate(
                remainingEffort = F('estimation') - F('logged'))
        context['object_list'] = context['object_list'].order_by('-priority_sum')
        return context

def add_dependency(request, task_pk, dependency_pk):
    if task_pk == dependency_pk:
        messages.error(request, 'A task can not get itself as dependency')
        return redirect('task-select-new-dependency', task_pk)
    task = Task.objects.get(pk = task_pk) #: :type task: Task
    dependency = Task.objects.get(pk = dependency_pk) #: :type task: Task
    transient_dependants = task.get_all_transient_dependants()
    if dependency in transient_dependants:
        messages.error(request, 'The choosen task depends on this task, so it can not be its dependency.')
        return redirect('task-select-new-dependency', task_pk)
    task.dependencies.add(dependency)
    task.full_clean()
    task.save()
    return redirect('task-edit', task_pk)

def remove_dependency(request, task_pk, dependency_pk):
    task = Task.objects.get(pk = task_pk) #: :type task: Task
    dependency = Task.objects.get(pk = dependency_pk) #: :type task: Task
    task.dependencies.remove(dependency)
    task.full_clean()
    task.save()
    dependency.full_clean()
    dependency.save()
    return redirect('task-edit', task_pk)

def logWork(request, taskPk):
    task = Task.objects.get(pk = taskPk)
    task.logged += 1
    task.save()
    return HttpResponse('Abgeschlossene Arbeit für "' + task.name + '" protokolliert.')

def prio_up(request, task_pk):
    # get task_to_raise
    task_to_raise = Task.objects.get(pk = task_pk)

    # get tasklist
    tasks = Task.objects.filter(status__is_done = False).order_by('-priority_sum')
    if 'sticky_scope' in request.session:
        sticky_scope = Scope.objects.get(pk = request.session['sticky_scope'])
        tasks = tasks.filter(Q(scope__isnull = True) | Q(scope = sticky_scope))
    
    try:
    # get task_to_raise one over the current
        task_to_beat = tasks.filter(priority_sum__gt=task_to_raise.priority_sum).last()
    
        # calculate the prio difference
        priority_difference = task_to_beat.priority_sum - task_to_raise.priority_sum
        
        # update the prio-points
        task_to_raise.priorityPoints += priority_difference + 1
        
        # clean and save
        task_to_raise.full_clean()
        task_to_raise.save()
        
        # redirect
        messages.success(request, 'Task raised for ' + str(priority_difference + 1) + ' points.')
    except:
        messages.error(request, 'Task was already highest.')

    try:
        next_url = request.GET['next']
    except:
        next_url = 'my-task-list'

    return redirect(next_url)

def task_done(request, taskPk):
    task = Task.objects.get(pk = taskPk) #: :type task: Task
    
    if task.is_repeating:
        new_state = Status.objects.get(is_new = True)
        if task.due_date != None:
            task.due_date = datetime.today() + timedelta(days = 1)
        task.logged = 0
        task.milestone = None
    else:
        new_state = Status.objects.get(is_done = True)    
    
    task.status = new_state
    task.save()
    
    try:
        next_url = request.GET['next']
    except:
        next_url = 'my-task-list'

    return redirect(next_url)