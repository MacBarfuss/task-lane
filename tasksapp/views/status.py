from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView

from tasksapp.models import Status

class StatusCreate(LoginRequiredMixin, CreateView):
    model = Status
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Status created successfully.')
        return super().form_valid(form)

class StatusUpdate(LoginRequiredMixin, UpdateView):
    model = Status
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Status saved successfully.')
        return super().form_valid(form)

class StatusDelete(LoginRequiredMixin, DeleteView):
    model = Status
    success_url = reverse_lazy('status-list')

class StatusList(LoginRequiredMixin, ListView):
    model = Status
