from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView

from tasksapp.models import Project, Scope

class ProjectCreate(LoginRequiredMixin, CreateView):
    model = Project
    fields = '__all__'

    def get_initial(self):
        sticky_scope = None
        if 'sticky_scope' in self.request.session:
            sticky_scope = self.request.session['sticky_scope']

        return {
            'scope': sticky_scope
        }
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Project created successfully.')
        return super().form_valid(form)

class ProjectUpdate(LoginRequiredMixin, UpdateView):
    model = Project
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Project saved successfully.')
        return super().form_valid(form)

class ProjectDelete(LoginRequiredMixin, DeleteView):
    model = Project
    success_url = reverse_lazy('project-list')


class ProjectList(LoginRequiredMixin, ListView):
    model = Project

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ProjectList, self).get_context_data(**kwargs)
        context['project_list'] = context['project_list'].filter(active=True)
        if 'sticky_project' in self.request.session:
            context['sticky_project'] = Project.objects.get(pk = self.request.session['sticky_project'])
        if 'sticky_scope' in self.request.session:
            context['sticky_scope'] = Scope.objects.get(pk = self.request.session['sticky_scope'])
            context['project_list'] = context['project_list'].filter(scope = context['sticky_scope'])
        return context
