from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

from django.urls import reverse_lazy

from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView

from tasksapp.models import Context

class ContextCreate(LoginRequiredMixin, CreateView):
    model = Context
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Context created successfully.')
        return super().form_valid(form)

class ContextUpdate(LoginRequiredMixin, UpdateView):
    model = Context
    fields = '__all__'
    
    def form_valid(self, form):
        # add success message
        messages.success(self.request, 'Context saved successfully.')
        return super().form_valid(form)

class ContextDelete(LoginRequiredMixin, DeleteView):
    model = Context
    success_url = reverse_lazy('context-list')

class ContextList(LoginRequiredMixin, ListView):
    model = Context
