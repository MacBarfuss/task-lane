from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic.edit import UpdateView

from tasksapp.models import UserSettings

class Update(LoginRequiredMixin, UpdateView):
    model = UserSettings
    fields = ['pomodoro_duration']

    def get_object(self):
        settings = UserSettings.objects.get(user = self.request.user)
        return settings
