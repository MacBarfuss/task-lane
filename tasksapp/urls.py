from django.urls import path
from django.views.generic import TemplateView

from tasksapp.views import context, milestone, project, scope, status, sticky, task, usersettings,\
    goal

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html"), name='tasks-index'),

    path('context/', context.ContextList.as_view(), name='context-list'),
    path('context/add/', context.ContextCreate.as_view(), name='context-add'),
    path('context/<int:pk>/', context.ContextUpdate.as_view(), name='context-edit'),
    path('context/<int:pk>/delete/', context.ContextDelete.as_view(), name='context-delete'),

    path('goal/', goal.ListView.as_view(), name='goal-list'),
    path('goal/add/', goal.CreateView.as_view(), name='goal-add'),
    path('goal/<int:pk>/', goal.UpdateView.as_view(), name='goal-edit'),
    path('goal/<int:pk>/delete/', goal.DeleteView.as_view(), name='goal-delete'),

    path('milestone/', milestone.MilestoneList.as_view(), name='milestone-list'),
    path('milestone/add/', milestone.MilestoneCreate.as_view(), name='milestone-add'),
    path('milestone/<int:pk>/', milestone.MilestoneUpdate.as_view(), name='milestone-edit'),
    path('milestone/<int:pk>/delete/', milestone.MilestoneDelete.as_view(), name='milestone-delete'),
    path('milestone/<int:pk>/finish/', milestone.MilestoneFinish.as_view(), name='milestone-finish'),
    path('milestone/<int:pk>/report/', milestone.Report.as_view(), name='milestone-report'),
    path('milestone/<slug:pk>/setup', milestone.Setup.as_view(), name='milestone-setup'),
    path('milestone/<int:milestone_pk>/add/task/<int:task_pk>', milestone.setup_add_task, name='milestone-add-task'),

    path('project/', project.ProjectList.as_view(), name='project-list'),
    path('project/add/', project.ProjectCreate.as_view(), name='project-add'),
    path('project/<int:pk>/', project.ProjectUpdate.as_view(), name='project-edit'),
    path('project/<int:pk>/delete/', project.ProjectDelete.as_view(), name='project-delete'),

    path('scope/', scope.ListView.as_view(), name='scope-list'),
    path('scope/add/', scope.CreateView.as_view(), name='scope-add'),
    path('scope/<int:pk>/', scope.UpdateView.as_view(), name='scope-edit'),
    path('scope/<int:pk>/delete/', scope.DeleteView.as_view(), name='scope-delete'),

    path('status/', status.StatusList.as_view(), name='status-list'),
    path('status/add/', status.StatusCreate.as_view(), name='status-add'),
    path('status/<int:pk>/', status.StatusUpdate.as_view(), name='status-edit'),
    path('status/<int:pk>/delete/', status.StatusDelete.as_view(), name='status-delete'),

    path('task/my/', task.MyTaskList.as_view(), name='my-task-list'),
    path('task/all/', task.AllTaskList.as_view(), name='all-task-list'),
    path('task/add/', task.TaskCreate.as_view(), name='task-add'),
    path('task/<int:pk>/', task.TaskUpdate.as_view(), name='task-edit'),
    path('task/<int:pk>/delete/', task.TaskDelete.as_view(), name='task-delete'),
    path('task/<int:task_pk>/select-new-dependency/', task.SelectNewDependency.as_view(), name='task-select-new-dependency'),
    path('task/<int:taskPk>/done', task.task_done, name='task-done'),
    path('task/<int:taskPk>/logWork', task.logWork, name='task-log-work'),

    path('usersettings/', usersettings.Update.as_view(), name = 'usersettings-edit'),

# special actions on tasks
    path('task/<int:task_pk>/split', task.TaskSplitView.as_view(), name = 'task-split'),
    path('task/<int:task_pk>/add-dependency/<int:dependency_pk>/', task.add_dependency, name = 'task-add-dependency'),
    path('task/<int:task_pk>/remove-dependency/<int:dependency_pk>/', task.remove_dependency, name = 'task-remove-dependency'),
    path('task/<int:task_pk>/prio-up', task.prio_up, name = 'task-prio-up'),

# handling stickiness
    path('sticky/project/<int:project_pk>/set', sticky.sticky_project_set, name = 'sticky-project-set'),
    path('sticky/project/all/unset', sticky.sticky_project_unset, name = 'sticky-project-unset-all'),
    path('sticky/scope/<int:scope_pk>/set', sticky.sticky_scope_set, name = 'sticky-scope-set'),
    path('sticky/scope/all/unset', sticky.sticky_scope_unset, name = 'sticky-scope-unset-all'),
]
