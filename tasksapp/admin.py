from django.contrib import admin

from tasksapp.models import Category, Context, Milestone, Project, Scope, Task, UserSettings,\
    Goal

admin.site.register(Category)
admin.site.register(Context)
admin.site.register(Goal)
admin.site.register(Milestone)
admin.site.register(Project)
admin.site.register(Scope)
admin.site.register(Task)
admin.site.register(UserSettings)