from django.apps import AppConfig


class TasksappConfig(AppConfig):
    name = 'tasksapp'
    label = 'tasksapp'
    verbose_name = 'Task-Lane'
