from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput

from django import forms

from tasksapp.models import Milestone, Project, Task, Goal

class GoalForm(forms.ModelForm):
    class Meta:
        model = Goal
        fields = '__all__'
        widgets = {
            'timebound': DatePickerInput(
                options = {
                    'locale': 'de',
                    'format': 'YYYY-MM-DD',
                }
            ),
        }

class MilestoneFinishForm(forms.ModelForm):
    class Meta:
        model = Milestone
        fields = ['name', 'description', 'datetime_to_pass', 'project']
        widgets = {
            'datetime_to_pass': DateTimePickerInput(
                options = {
                    'locale': 'de',
                    'format': 'YYYY-MM-DD HH:mm',
                }
            )
        }

    project = forms.models.ModelChoiceField(queryset = Project.objects.all(), disabled = True, required = False)


class MilestoneForm(forms.ModelForm):
    class Meta:
        model = Milestone
        fields = '__all__'
        widgets = {
            'datetime_to_pass': DateTimePickerInput(
                options = {
                    'locale': 'de',
                    'format': 'YYYY-MM-DD HH:mm',
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'initial' in kwargs:
            self.fields['project'].queryset = Project.objects.filter(active=True)


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = '__all__'
        widgets = {
            'due_date': DatePickerInput(
                options = {
                    'locale': 'de',
                    'format': 'YYYY-MM-DD',
                }
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'initial' in kwargs:
            self.fields['milestone'].queryset = Milestone.objects.filter(happened=False)
            self.fields['project'].queryset = Project.objects.filter(active=True)


class TaskSplitForm(forms.Form):
    name = forms.CharField(max_length=200)
