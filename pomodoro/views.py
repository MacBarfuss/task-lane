from django.views import generic

from tasksapp.models import Task, UserSettings

class IndexView(generic.TemplateView):
    template_name = 'pomodoro/index.html'
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)

        settings = UserSettings.objects.get_or_create(user = self.request.user)[0]

        context['pomoMinutes'] = settings.pomodoro_duration

        taskPk = self.request.GET.get('pomoTask')
        context['pomoTask'] = Task.objects.filter(pk = taskPk).first()

        return context
