function CountDownTimer(duration, granularity) {
    this.duration = duration;
    this.granularity = granularity || 1000;
    this.haltFtns = [];
    this.resumeFtns = [];
    this.startFtns = [];
    this.stopFtns = [];
    this.tickFtns = [];
    this.running = false;
    this.startTime = undefined;
}

CountDownTimer.prototype.start = function() {
    if (this.running) {
        return;
    }
    this.running = true;
    this.startTime = Date.now();
    var that = this,
        diff, obj;

    that.startFtns.forEach(function(ftn) {
        ftn.call(this);
    }, that);

    (function timer() {
        diff = that.duration - (((Date.now() - that.startTime) / 1000) | 0);

        if (diff > 0) {
            setTimeout(timer, that.granularity);
        } else {
            diff = 0;
            that.running = false;
            that.stopFtns.forEach(function(ftn) {
                ftn.call(this);
            }, that);
        }

        obj = CountDownTimer.parse(diff);
        that.tickFtns.forEach(function(ftn) {
            ftn.call(this, obj.minutes, obj.seconds);
        }, that);
    }());
};

CountDownTimer.prototype.halt = function() {
    this.running = false;
    diff = this.duration - (((Date.now() - this.startTime) / 1000) | 0);
    this.duration = diff;
    this.startTime = undefined;
    this.haltFtns.forEach(function(ftn) {
        ftn.call(this);
    }, this);
}

CountDownTimer.prototype.onHalt = function(ftn) {
    if (typeof ftn === 'function') {
        this.haltFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.onResume = function(ftn) {
    if (typeof ftn === 'function') {
        this.resumeFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.onStart = function(ftn) {
    if (typeof ftn === 'function') {
        this.startFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.onStop = function(ftn) {
    if (typeof ftn === 'function') {
        this.stopFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.onTick = function(ftn) {
    if (typeof ftn === 'function') {
        this.tickFtns.push(ftn);
    }
    return this;
};

CountDownTimer.prototype.expired = function() {
    return !this.running;
};

CountDownTimer.parse = function(seconds) {
    return {
        'minutes': (seconds / 60) | 0,
        'seconds': (seconds % 60) | 0
    };
};
